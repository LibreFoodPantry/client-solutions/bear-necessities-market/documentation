# Bear Necessities

This project contains documentation relevant to the overall BNM project. The requirements and design for the individual subproject will be located with the individual subprojects.

## Board Organization
All boards within this project should have the following columns:
* **Status Info** - This is the location of Working Agreements, Team Velocity, and sprint Retrospectives.
* **Status Project Backlog** - This is where all open issues that are not assigned to a specific sprint should be lodged.
* **Status Sprint Backlog** - This is where the issues that will be worked on within a sprint should be located at the beginning of the sprint.
* **Status In Process** When one or more team members begin to work on an issue that is located in the Sprint Backlog, that issue should then be moved to this column.
* **Status Group Review** Once an issue is considered complete, that issue should be moved to either this column so that someone from the team can review the issue or if the issue does not need team review, it should be moved into the "Needs Review" column.
* **Status Needs Review** This column is the location for issues that are ready for review by the Project Manager (course instructor). Once an issue has undergone a review by one or more team members, the issue should be moved into this column.
* **Status Done** This column is only for the use of the Project Manager (course instructor).  Once the project manager has signed off that an issue is complete, the project manager will move an item into this column.
* **Closed** - This column should be made visible by clicking the "Edit Board" button and checking the "Show the Closed list." checkbox.

### [Architecture](Architecture.md)
A high-level design for the pieces of the software system and how they interact with each other.

---
Copyright &copy; 2023 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
