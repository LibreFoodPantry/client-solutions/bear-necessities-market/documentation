# Deployment Guide

The Bear Necessities Market utilizes a set of software that is required for the software to function, which need to be downloaded first. Basic user instructions are also included in this guide.

## Required Software

The software listed here must be downloaded for the system to run.

* [**Docker Desktop**](https://www.docker.com/products/docker-desktop/) - Creates and runs containers for BNM software to run in.
* [**GuestInfoIntegration**](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/guestinfosystem/guestinfointegration) - A BNM module that is responsible for downloading, installing, and running the system.

## Installation

### Installing Docker Desktop & Windows Subsystem for Linux
1. Install Windows Subsystem for Linux by starting an admin-privileged command prompt shell, and typing `wsl --install`.
1. Ensure the windows features 'Containers', 'Virtual Machine Platform', and 'Windows Subsystem for Linux' are enabled.
    * This can be done by pressing the Windows key and typing 'Turn Windows Features on and off'.
    * Admin credentials are required.
1. Follow the instructions on [Docker's website](https://docs.docker.com/desktop/install/windows-install/) to install Docker Desktop.

### Downloading GuestInfoSystem
1. Download GuestInfoIntegration from [here](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/guestinfosystem/guestinfointegration/-/archive/main/guestinfointegration-main.zip).
2. Extract the contents of this archive to `C:\Users\Public\BNM\GuestInfoSystem`. The BNM and GuestInfoSystem folders likely do not already exist so you will have to create them.
    * The 'Public' user folder is being used as it is a folder that can be accessed by all users.
    * Running `run.cmd` here also ensures each user has access to the database information.

### Configuring Docker for Domain Users
1. Ensure that Docker Desktop is installed.
2. Open an elevated command prompt. This can be done by pressing the Windows key, entering 'cmd' into the search, and then clicking "Run As Administrator" when Command Prompt shows up. Enter admin credentials if they are needed.
3. Once in the command prompt, enter `net localgroup docker-users /add "Domain Users"`.
4. Ensure this has worked by logging in as a domain user not in the docker-users group, and attempt to start Docker Desktop.

### Adding a task in Task Scheduler to have Docker Desktop run when a user logs on
1. Launch Task Scheduler. If you are not logged in as an administrator, run it with administrative privleges and input the admin credentials if necessary.
2. On the top right of the program, click 'Create Task'.
3. Input a name, preferably a simple one such as 'Start-Docker'.
4. Configure the task to run as the logged in user. This can be done by:
    * Click "Change User or Group"
    * Under 'Enter the object name to select' in the window that opened, enter 'Users'.
    * Click 'Check Names' to verify it works. The 'Users' should change to the name of your local machine, collowed by '\Users'
    * Hit OK.
5. Go to the triggers tab, and hit "New...".
6. In the window that pops up, change "Begin the task:" to "At log on". Verify the setting is set to "Any user" and not "Specific user".
7. Hit OK.
8. Go to the Actions tab, and hit "New...".
9. Verify the action is set to "Start a program".
10. Click "Browse" on the "program/script" field. Navigate to `C:\Program Files\Docker\Docker` and select "Docker Desktop.exe".
11. Click OK.
12. Click "OK" in the Create Task window to create the task.
13. Reboot to verify that Docker Desktop runs as soon as a domain user logs in.

### Configuring GuestInfoIntegration to run on user login
1. Create a file, preferably named 'start-BNM.bat' in your Documents folder.
2. Modify it to include the following lines: 

`ping -n 15 127.0.0.1 1>nul`

`cd C:\Users\Public\BNM\GuestInfoSystem\Windows`

`START /MIN CMD.EXE /C run.cmd`
* This effectively delays for 14 seconds before running run.cmd, which will start our containers.
    * Depending on your system configuration, you may need to increase this delay, or you may be able to decrease it. 
    * The timing was done in reference to when Docker Desktop is started and the Docker Engine is running.
3. Copy this file to your clipboard.

### Onboarding a new user
Currently, the way Docker is configured requires inputting admin credentials 2-3 times when a new user attempts to use the deployed computer, specifically if this is their first time logging into the computer via domain. To onboard a new user:
1. Have the user login.
2. Docker should ask to configure itself. If it asks to automatically update, select that.
3. Input admin credentials as many times as is needed.
4. Logout and login as the same user to verify it works, and to start the containers.

## Operation Instructions

1. To open the console, click on the command prompt tab in the task bar.
2. In the command prompt menu, you may enter:
   * ```1``` to start the webpage interface.
   * ```2``` to view the system logs.
   * ```3``` to update the system.
   * ```4``` to stop the system.

### Other Notes

* Running ```run.cmd``` for the first time creates a container. Running the script again after stopping the container should restart the same container, preserving any data in the system. 
