# BNM User Stories
The following User Stories define the MVP for BNM as of 2024-02-22:

### Staff 1 – Guest Verification
As a staff member, I want to be able to verify that the guest is a member of the BNM via their BNM card so that they can obtain food from the pantry.

**Confirmation:**
* Guest provides BNM ID and indicates graduation year, whether they are a commuter or live on campus, graduation year and whether they are a grad student or not which is entered by the staff.
* IDs are restricted to a specific format of 9 alphanumeric characters in any order.
* The system validates the entry and returns to the main screen.
* Note that there is no check for incorrectly entered IDs other than incorrect format.

### Staff 2 – Log Incoming Inventory
As a staff member, I want to be able to log the inventory that comes into the pantry into the system so that I can identify needed items and items for which there is an excess. 

**Confirmation:** 
* As each item enters the pantry, the UPC code is either scanned or manually entered. 
* If the item is successfully found, the system returns to the scanning screen.
* If the item is not found, a message is displayed.

### Staff 3 – Identify Expired Inventory
As a staff member or administrator, I want to be able to identify expired inventory in the pantry so that items may be removed, guests do not obtain expired food and to meet federal food safety guidelines. 

**Confirmation:**
* The staff member enters the date by which they would like to check for expired items in the pantry
* The staff member is presented with a report that shows the item UPC, expiration date, and description 
* If no items exist that are expiring, the system presents a blank report

### Staff 4 – Remove Expired Inventory 
As a staff member or administrator, I want to be able to remove expired inventory from the system so that guests do not obtain expired food and to meet federal food safety guidelines. 

**Confirmation:**
* The staff member enters the date by which all expired food should be removed from the inventory 
* The staff member is presented with a report of all UPC codes, expiration dates, and descriptions of items removed from inventory 

### Admin 1 – Generate Guest Visit Report
As an administrator, I want to be able to generate a report of all pantry visits between a specific start and end date so that I can understand pantry usage patterns and provide information to the Board of Trustees. 

**Confirmation:**
* The staff member enters the beginning and end dates for which to report pantry visits
* The staff member is presented with a report of all pantry visits including guest ID, grad or undergrad, commuter status and graduation year
* If no visits occurred, the administrator should be presented with an empty report. 

### Admin 2 – Generate Inventory Report
As an administrator, I want to be able to generate a report of all current inventory in the pantry at a particular point in time so that I can anticipate pantry needs and surpluses. 

**Confirmation:**
* The administrator enters the date by for which the inventory should be reported
* The administrator is presented with a report of all UPC codes and descriptions of items in the pantry on that date
* If the pantry is empty, the staff member receives an empty report  
