# Architecture of Bear Necessities Market Client Solution

The Bear Necessities Market client solution uses a microservices architecture. 


## Features

Each system is composed of features, which use components:
* IAMSystem (Third-party)
  * Login (feature)
* PlaceOrderSystem
  * PlaceOrder (feature) which uses:
    * PlaceOrderWebUI (component)
    * PlaceOrderService (component)
* ViewOrderSystem
  * ViewOrder (feasture) which uses:
  * ViewOrderWebUI (component)
  * ViewOrderService (component)

## Architecture Diagram 
```plantuml
@startuml
Actor Guest
Actor Admin

node "ViewBrowser" {
    [ViewOrderFrontEnd]
    [Login]
    [ManageUsers]
}

node "OrderBrowser" {
    [PlaceOrderFrontEnd]
    [Login]
    [ManageUsers]
}


package "OrderSystem" {
    node ViewOrderWebServer <<Server>>
    node PlaceOrderWebServer <<Server>>
    node OrderAPI <<Server>>
    node OrderAPI <<Server>>
    database OrderDb <<Server>>
    OrderAPI --> OrderDb
}

package "IAMSystem" {
}

Guest --> OrderBrowser
Admin --> ViewBrowser
PlaceOrderFrontEnd --> OrderAPI
ViewOrderFrontEnd --> OrderAPI
ViewOrderWebServer ..> ViewOrderFrontEnd
PlaceOrderWebServer ..> PlaceOrderFrontEnd

IAMSystem ..> Login
IAMSystem --> Login
IAMSystem ..> ManageUsers
IAMSystem --> ManageUsers

@enduml
```


---
Copyright &copy; 2021 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
